/*******************************************
*
*   Datei:		tree.h
*
*******************************************/

#ifndef TREE_H
#define TREE_H

#include <stdlib.h>
#include <iostream>
#include <string>
#include <time.h>
#include <math.h>
#include <stdexcept>
#include <fstream>

#include "TreeNode.h"

using namespace std;

//Klasse Tree
template<class t>
class Tree
{
public:
    Tree(TreeNode<t>* wurzel);
    TreeNode<t>* NewTreeNode(int key, string info);
    TreeNode<t>* getWurzel();
    TreeNode<t>* TreeInsert(t key, string info);
    TreeNode<t>* TreeGenerateRandomTree(int n, int seed);
    TreeNode<t>* TreeGenerateRandomTreeTest(int n, int seed);
    TreeNode<t>* TreeSearch(int key);
    TreeNode<t>* TreeRightRotate(TreeNode<t>* rotateThisNode);
    TreeNode<t>* TreeMinimum(TreeNode<t>* node);
    void deleteTreeNode(t key);
    void transplant(TreeNode<t>* u, TreeNode<t>* v);
    void TreePrint();
    void TreePrint(TreeNode<t>* node, int indentation);
    void TreeSave(string);
    void TreeSave(TreeNode<t>* node, string);
    bool SaveLevel(TreeNode<t>* node, int level, string);
    void DeleteTree();
    int ReadTree(string is);
    void DeleteTreeData(string);
    void DeleteTree(TreeNode<t>* node);
private:
    TreeNode<t>* wurzel;
};

/**
* Konstruktor
*
**/
template<class t>
Tree<t>::Tree(TreeNode<t>* wurzel)
{
    this->wurzel = wurzel;
}

/**
* getter-Methode f�r die Wurzel
**/
template<class t>
TreeNode<t>* Tree<t>::getWurzel()
{
    return wurzel;
}

/**
* Erstellen eines neuen Knoten
*
* Parameter: der key und die Info
**/
template<class t>
TreeNode<t>* Tree<t>::NewTreeNode(int key, string info)
{
    TreeNode<t>* node = new TreeNode<t>(key, info);
    return(node);
}

/**
* F�gt einen neuen knoten ein
*
* Es wird der richtige Platz f�r den neuen knoten gesucht und dann dort eingef�gt
*
* Parameter der hinzuzuf�gende Schl�ssel und die Info des schl�ssels
*
**/
template<class t>
TreeNode<t>* Tree<t>::TreeInsert(t key, string info)
{
    TreeNode<t>* newTree = NewTreeNode(key, info);

    if (wurzel == nullptr || wurzel->getKey() == 0)
    {
        wurzel = newTree;
        return wurzel;
    }

    TreeNode<t>* currentTree = wurzel;

    while (1)
    {
        if (key <= currentTree->getKey())
        {
            if (currentTree->getLeft() != NULL)
            {
                currentTree = currentTree->getLeft();
            }
            else
            {
                currentTree->setLeft(newTree);
                newTree->setUp(currentTree);
                break;
            }
        }
        else if (key > currentTree->getKey())
        {
            if (currentTree->getRight() != NULL)
            {
                currentTree = currentTree->getRight();
            }
            else
            {
                currentTree->setRight(newTree);
                newTree->setUp(currentTree);
                break;
            }
        }
    }
    return wurzel;
}

/**
* Rotiert den baum an einer zu w�hlenden stelle, falls dies m�glich ist
*
* Parameter: der zu drehende knoten
*
**/
template<class t>
TreeNode<t>* Tree<t>::TreeRightRotate(TreeNode<t>* rotateThisNode)
{
    TreeNode<t>* y = rotateThisNode;
    TreeNode<t>* x;
    TreeNode<t>* zwischenspeicher;
    if (y->getKey() == NULL)
    {
        cout << "ERROR! y == NULL";
        return NULL;
    }

    if (y->getLeft() != NULL)
    {
        x = y->getLeft();
    }
    else
    {
        cout << "ERROR! y->Left() == NULL";
        return NULL;
    }

    if (x->getKey() == NULL)
    {
        cout << "ERROR! x == NULL";
        return NULL;
    }

    y->setLeft(x->getRight());

    if (x->getRight() != NULL)
    {
        y->getLeft()->setUp(y);
    }

    x->setUp(y->getUp());

    if (y->getUp() == NULL)
    {
        wurzel = x;
    }
    else if (y->getUp()->getLeft() == y)
    {
        y->getUp()->setLeft(x);
    }
    else
    {
        y->getUp()->setRight(x);
    }
    x->setRight(y);
    y->setUp(x);
}

/**
* Methode um die Print-Methode mit dem Objekt aufzurufen
**/
template<class t>
void Tree<t>::TreePrint()
{
    cout << "\n\n";
    TreePrint(getWurzel(), 0);
}

/**
* Methode um den Suchbaum auf der Konsole auszugeben
*
* Parameter: der Knoten und die Ebene
**/
template<class t>
void Tree<t>::TreePrint(TreeNode<t>* node, int indentation)
{
    int i;
    if (node != NULL && node->getKey() != 0)
    {
        TreePrint(node->getRight(), indentation + 1);

        for (i = 0; i < indentation; i++)
        {
            cout << "\t";
        }

        cout << "(key: " << node->getKey() << ", info: '" << node->getInfo() << "')\n\n\n";
        TreePrint(node->getLeft(), indentation + 1);
    }
    return;
}

/**
* Generiert einen zuf�lligen baum
*
* Parameter: Die gr��e des baumes und der seed f�r die zufallszahlen
*
**/
template<class t>
TreeNode<t>* Tree<t>::TreeGenerateRandomTree(int n, int seed)
{
    int r = 0;
    srand(seed);
    TreeNode<t>* tree = NULL;
    for (int i = 1; i <= n; i++)
    {
        r = (rand() % 100 + 1);
        tree = TreeInsert(r, "kwt");
    }
    return 0;
}

/**
* Generiert den immer selben zuf�lligen baum
*
* Parameter: Die gr��e des baumes und der seed f�r die zufallszahlen
*
**/
template<class t>
TreeNode<t>* Tree<t>::TreeGenerateRandomTreeTest(int n, int seed)
{
    int r = 0;
    srand(5);
    TreeNode<t>* tree = NULL;
    for (int i = 1; i <= n; i++)
    {
        r = (rand() % 100 + 1);
        tree = TreeInsert(r, "kwt");
    }
    return 0;
}

/**
* Suchen eines Schl�ssels in einen Baum.
* Pr�fen ob der Baum leer ist
* Vergleicht den gesuchten Schl�ssel mit den aktuellen Schl�ssel
*
* parameter gesuchter Schl�ssel
*
* return gibt den gefundenen Schl�ssel zur�ck.
**/
template<class t>
TreeNode<t>* Tree<t>::TreeSearch(int key)
{
    TreeNode<t>* current = wurzel;
    int zaehler = 0;
    if (wurzel != NULL)
    {
        while (current != NULL && current->getKey() != key)
        {
            if (key < current->getKey())
            {
                current = current->getLeft();
                zaehler++;
            }
            else
            {
                current = current->getRight();
                zaehler++;
            }
        }

        if (current == NULL)
        {
            cout << "Knoten wurde nicht gefunden." << endl;
            return 0;
        }
        else
        {
            cout << "Vergleiche um Schluessel zu finden: " << zaehler << endl;
        }
    }
    else
    {
        cout << "Liste leer." << endl;
    }
    return current;
}

/**
* Speichert den aktuell geladenen baum in einer Datei ab.
*
* Parameter: der Knoten, das jeweilige level auf dem wir uns befinden und der Dateiname
*
**/
template<class t>
bool Tree<t>::SaveLevel(TreeNode<t>* node, int level, string s)
{
    fstream f;

    f.open(s, ios::app);

    if (node == nullptr)
        return false;

    if (level == 1)
    {
        f << node->getKey() << " " << node->getInfo() << endl;

        // return true if at-least one node is present at given level
        return true;
    }

    bool left = SaveLevel(node->getLeft(), level - 1, s);
    bool right = SaveLevel(node->getRight(), level - 1, s);

    return left || right;
    f.close();
}

/**
* Methode f�rs speichern
*
* Parameter: den Knoten und Dateiname
**/
template<class t>
void Tree<t>::TreeSave(TreeNode<t>* node, string s)
{
    // start from level 1 -- till height of the tree
    int level = 1;

    // run till printLevel() returns false
    while (SaveLevel(node, level, s))
        level++;
}

/**
* Speicher-Methode um die Methode, mit dem Objekt, f�rs speichern aufzurufen
*
* Parameter: Dateiname
**/
template<class t>
void Tree<t>::TreeSave(string s)
{
    TreeSave(getWurzel(), s);
}

/**
* Liest einen Knoten aus einer textdatei ein und baut daraus einen neuen baum auf.
*
*
* Parameter: Dateiname
*
**/
template<class t>
int Tree<t>::ReadTree(string is)
{

    fstream datei;

    string info;

    int schlussel;

    DeleteTree();

    datei.open(is, ios::in);

    while (!datei.eof())
    {

        datei >> schlussel;
        datei >> info;

        TreeInsert(schlussel, info);

    }

    datei.close();

    return 0;
}

/**
* Methode um den Inhalt einer Datei zu leeren
*
* Parameter: Dateiname
**/
template<class t>
void Tree<t>::DeleteTreeData(string s)
{
    fstream f;

    f.open(s, ios::out);

    f.close();
}

/**
* Methode um die Delete-Methode, mit dem Objket, aufzurufen
**/
template<class t>
void Tree<t>::DeleteTree()
{
    DeleteTree(getWurzel());
}

/**
* L�schet den momentanen Baum
*
* Parameter: Wurzel
**/
template<class t>
void Tree<t>::DeleteTree(TreeNode<t>* node)
{
    int i;

    if (node != NULL && node->getKey() != 0)
    {
        DeleteTree(node->getRight());
        DeleteTree(node->getLeft());

        if (node != wurzel)
        {
            free(node);
        }
        else
        {
            wurzel->setKey(NULL);
        }
    }

    return;
}

/**
* L�scht einen Knoten aus einem Baum und f�gt die daraus enstehenden Teilb�ume wieder zusammen.
* Pr�ft:
*       ob der zu l�schende Schl�ssel vorhanden ist
*       Fall 1 wenn nur ein Nachfolgerbaum existiert wird diese an die Stelle des gel�schten Knoten gesetzt
*       Fall 2 wenn zwei Nachfolgerb�ume existieren wird vom rechten Teilbaum der kleinste Konten an die Stelle
*              gel�schte Knoten gesetzt und der linke Teilbaum wird an den kleinsten Konten geh�ngt.
*
* Parameter der zu l�schende Schl�ssel
*
**/
template <class t>
void Tree<t>::deleteTreeNode(t key)
{
    try
    {
        if (this->TreeSearch(key) != nullptr)
        {
            TreeNode<t>* node = this->TreeSearch(key);
            // wenn eine Seite nicht existiert, dann wird die anderen hochgeschoben
            if (node->getLeft() == nullptr)
            {
                transplant(node, node->getRight());
            }
            else if (node->getRight() == nullptr)
            {
                transplant(node, node->getLeft());
            }
            // vom zu l�schenden Knoten der rechte Teilbaum davon den kleinsten Wert suchen und an die Position des zu l�schenden Knoten setzen
            else
            {
                TreeNode<t>* y = TreeMinimum(node->getRight());
                if (y->getUp() != node)
                {
                    transplant(y, y->getRight());
                    y->setRight(node->getRight());
                    y->getRight()->setUp(y);
                }
                transplant(node, y);
                y->setLeft(node->getLeft());
                y->getLeft()->setUp(y);
            }
        }
    }
    catch (exception& e)
    {
        cerr << e.what() << endl;
    }
}

/**
* entfernt den Teilbaum u und ersetzt diesen durch den Teilbaum v.
* Pr�ft:
*       u die Wurzel ist wenn ja dann wird v zu Wurzel
*       u linker oder rechter nachfolger wenn linker dann ist v der linke nachfolger von den vorg�nger von u
*       sonst ist v der rechte nachfolger von den vorg�nger von u
*       wenn v  existiert dann ist der vorg�nger von v gliech der vorg�nger von u
* parameter Teilbaum u und Teilbaum v
*
**/
template <class t>
void Tree<t>::transplant(TreeNode<t>* u, TreeNode<t>* v)
{
    if (u->getUp() == NULL)
        wurzel = v;
    else if (u == u->getUp()->getLeft())
        u->getUp()->setLeft(v);
    else
        u->getUp()->setRight(v);
    if (v != nullptr)
        v->setUp(u->getUp());
}

/**
* Ermittelt den kleinsten Knoten eines Baumes/Teilbaumes
*
* Parameter Knoten von dem das min gesucht wird
*
* return gibt kleinsten Knoten zur�ck
*
**/
template <class t>
TreeNode<t>* Tree<t>::TreeMinimum(TreeNode<t>* node)
{
    while (node->getLeft() != nullptr)
        node = node->getLeft();
    return node;
}

#endif
