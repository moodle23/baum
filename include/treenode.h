/*******************************************
*
*   Datei:		treenode.h
*
*******************************************/

#ifndef TREENODE_H
#define TREENODE_H

#include <string>

using namespace std;

/**
* Klasse: TreeNode
**/
template<class t>
class TreeNode
{
public:
    TreeNode(t key, string info);

    TreeNode<t>* getUp();
    void setUp(TreeNode<t>* up);
    TreeNode<t>* getLeft();
    void setLeft(TreeNode<t>* left);
    TreeNode<t>* getRight();
    void setRight(TreeNode<t>* right);
    string getInfo();
    int getKey();
    void setKey(int key);

private:
    TreeNode<t>* up;
    t key;
    string info;
    TreeNode<t>* left;
    TreeNode<t>* right;
};

#endif // TREENODE_H

/**
* Konstruktor
**/
template <class t>
TreeNode<t>::TreeNode(t key, string info)
{
    this->key = key;
    this->info = info;
    this->up = nullptr;
    this->left = nullptr;
    this->right = nullptr;
}

/**
* getter f�r up (Vorheriger Knoten)
**/
template<class t>
TreeNode<t>* TreeNode<t>::getUp()
{
    return up;
}

/**
* setter f�r up (Vorheriger Knoten)
**/
template<class t>
void TreeNode<t>::setUp(TreeNode<t>* up)
{
    this->up = up;
}

/**
* getter f�r den rechten Nachfolgenden Knoten
**/
template<class t>
TreeNode<t>* TreeNode<t>::getRight()
{
    return right;
}

/**
* setter f�r den rechten Nachfolgenden Knoten
**/
template<class t>
void TreeNode<t>::setRight(TreeNode<t>* right)
{
    this->right = right;
}

/**
* getter f�r den linken Nachfolgenden Knoten
**/
template<class t>
TreeNode<t>* TreeNode<t>::getLeft()
{
    return left;
}

/**
* setter f�r den linken Nachfolgenden Knoten
**/
template<class t>
void TreeNode<t>::setLeft(TreeNode<t>* left)
{
    this->left = left;
}

/**
* getter f�r den Key (Schl�ssel)
**/
template<class t>
int TreeNode<t>::getKey()
{
    return key;
}

/**
* getter f�r die Info
**/
template<class t>
string TreeNode<t>::getInfo()
{
    return info;
}

/**
* setter f�r den Key (Schl�ssel)
**/
template<class t>
void TreeNode<t>::setKey(int key)
{
    this->key = key;
}
