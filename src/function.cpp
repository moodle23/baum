/*******************************************
*
*   Datei:		function.cpp
*
*******************************************/

/// Bibliothek einbinden
#include <iostream>

/// Klasse einbinden
#include "function.h"

/// Namespace Global defenieren
using namespace std;

/**
* Funktion: Banner
*
* Gibt ein Banner aus
**/
void Banner()
{

    cout << "Bin\x84re Suchb\x84ume" << endl;

}

/**
* Funktion: Men�
*
* Gibt ein Men� bzw das Unterbanner aus
**/
void Menue(int a)
{

    if (a == 0)
    {

        cout << "Hauptmen\x81" << endl;
        cout << "1 - Datei einlesen" << endl;
        cout << "2 - Bin\x84ren Suchbaum erstellen" << endl;
        cout << "3 - Zuf\x84lligen Baum erstellen" << endl;
        cout << "4 - Baum ausgeben" << endl;
        cout << "5 - Knoten suchen" << endl;
        cout << "6 - Knoten hinzuf\x81gen" << endl;
        cout << "7 - Rechtsrotation" << endl;
        cout << "8 - Knoten l\x94schen" << endl;
        cout << "9 - Baum l\x94schen" << endl;
        cout << "t - Tests" << endl;
        cout << "s - Baum speichern" << endl;
        cout << "e - Exit" << endl;
        cout << "Bitte ausw\x84hlen." << endl;
        cout << "Auswahl: ";

    }

    else if (a == 1)
    {

        cout << "1 - Datei einlesen" << endl;

    }

    else if (a == 2)
    {

        cout << "2 - Bin\x84ren Suchbaum erstellen" << endl;

    }

    else if (a == 3)
    {

        cout << "3 - Zuf\x84lligen Baum erstellen" << endl;

    }

    else if (a == 4)
    {

        cout << "4 - Baum ausgeben" << endl;

    }

    else if (a == 5)
    {

        cout << "5 - Knoten suchen" << endl;

    }

    else if (a == 6)
    {

        cout << "6 - Knoten hinzuf\x81gen" << endl;

    }

    else if (a == 7)
    {

        cout << "7 - Rechtsrotation" << endl;

    }

    else if (a == 8)
    {

        cout << "8 - Knoten l\x94schen" << endl;

    }

    else if (a == 9)
    {

        cout << "9 - Baum l\x84schen" << endl;

    }

}
