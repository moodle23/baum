/********************************************************
*                                                       *
*   Name:	    Nico Rohde-Falkenstein                  *
*   Gruppe:     23                                      *
*   Aufgabe:    IT-B�ume erstellen, lesen und ausgeben  *
*                                                       *
********************************************************/

/*******************************************
*
*   Datei:		main.cpp
*
*******************************************/

/// Bibliotheken einbinden
#include <iostream>
#include <Windows.h>
#include <conio.h>
#include <fstream>
#include <time.h>

/// Klassen einbinden
#include "tree.h"
#include "function.h"

/// Namespace Global defenieren
using namespace std;

/// Hauptprogramm
int main()
{

    try
    {

        char menu;

        Tree<int> tree(new TreeNode<int>(0, " "));

        do
        {

            system("cls");

            Banner();
            Menue(0);

            menu = _getch();

            if (menu == '1')        // Aus Datei �ffnen
            {

                system("cls");

                string dateiname;

                Banner();
                Menue(1);

                cout << "Bitte geben Sie die Datei an.\nDateiname:" << endl;
                cin >> dateiname;

                ifstream ifs("C:/Users/hdron/Desktop/Studium/" + dateiname, ifstream::in);

                if (!ifs.is_open())
                {

                    cout << endl << "Datei konnte nicht ge \b\x94 \bffnet werden!" << endl;

                    system("PAUSE");

                }

                else
                {

                    tree.ReadTree(dateiname);

                }

            }

            if (menu == '2')            // Suchbaum erstellen
            {

                system("cls");

                string wurzel = "Wurzel";

                int zahl = 0;
                int cnt_info = 0;
                int k = 0;

                Banner();
                Menue(2);

                cout << "Wie viele Knoten wollen Sie einf\x81gen?" << endl;
                cout << "Eingabe: ";
                cin >> k;

                // Eingabe der Wurzel
                cout << endl << "Geben Sie eine Zahl f\x81r die Wurzel ein." << endl;
                cout << "Eingabe: ";
                cin >> zahl;
                tree.TreeInsert(zahl, wurzel);	// F�gt die Wurzel ein

                // Schleife f�r das einf�gen der Knoten
                do
                {
                    cout << endl << "Geben Sie eine Zahl ein um diese in den Suchbaum einzuf\x81gen." << endl;
                    cout << "Eingabe: ";
                    cin >> zahl;
                    cnt_info++;
                    tree.TreeInsert(zahl, to_string(cnt_info));
                }
                while (k > cnt_info);
            }

            if (menu == '3')            // Zuf�lligen Suchbaum erstellen
            {
                system("cls");
                int k = 0;
                int r = (rand() % 1000 + 1);

                Banner();
                Menue(3);

                cout << "Wie viele Knoten soll der Suchbaum haben?" << endl;
                cout << "Eingabe: ";
                cin >> k;

                tree.TreeGenerateRandomTree(k, r);

            }

            if (menu == '4')            // Ausgabe des Suchbaums
            {

                system("cls");

                Banner();
                Menue(4);
                tree.TreePrint();

            }

            if (menu == '5')            // Knoten suchen
            {
                system("cls");
                int zahl = 0;

                Banner();
                Menue(5);

                cout << "Welchen Knoten wollen Sie suchen?" << endl;
                cout << "Eingabe: ";
                cin >> zahl;

                tree.TreeSearch(zahl);


            }

            if (menu == '6')            // Knoten hinzuf�gen
            {
                system("cls");
                int key = 0;
                string info = "kwt";

                Banner();
                Menue(6);

                cout << endl << "Geben Sie eine Zahl ein um die in die Suchbaum einzufuegen." << endl;
                cout << "Eingabe: ";
                cin >> key;

                tree.TreeInsert(key, info);

            }

            if (menu == '7')            // Rechtsrotation
            {
                system("cls");
                int key = 0;

                Banner();
                Menue(7);

                tree.TreePrint();

                cout << "Welchen Knoten wollen Sie rotieren?" << endl;
                cout << "Eingabe: ";
                cin >> key;

                tree.TreeRightRotate(tree.TreeSearch(key));

            }

            if (menu == '8')            // Knoten l�schen
            {
                system("cls");
                int key = 0;

                Banner();
                Menue(8);

                cout << "Welchen Knoten wollen Sie l\x94schen?" << endl;
                cin >> key;

                tree.deleteTreeNode(key);

            }

            if (menu == '9')            // Suchbaum l�schen
            {

                tree.DeleteTree();

            }

            if (menu == 's')            //Suchbaum speichern
            {
                string s = " ";
                cout << menu << endl << endl;
                cout << "Geben Sie den Namen der Datei ein!" << endl;
                cout << "Eingabe: ";
                cin >> s;

                tree.TreeSave(s);

                cout << endl << "Datei " + s + " wurde erstellt!" << endl;

            }

            if (menu == 't')            // Tests
            {

                system("cls");

                float zeit;

                int k = 0;
                int s = 0;
                int h = 0;
                int r = (rand() % 1000 + 1);

                Banner();

                cout << "Test: Laufzeit eines Baumes." << endl;
                cout << "Geben sie eine Startanzahl an." << endl;
                cin >> k;

                cout << "Wie viele Schritte sollen gemacht werden?" << endl;
                cin >> s;

                cout << "Wie hoch sollen die Schritte sein?" << endl;
                cin >> h;

                for(int i=1; i<=s; i++)
                {

                    zeit=clock();

                    tree.TreeGenerateRandomTreeTest(k, r);

                    zeit=clock()-zeit;

                    cout << i << ":" << endl;
                    cout << "\tDie ben\x94tigte Zeit f\x81r die Erstellung der " << k << " Knoten war: " << zeit << "ms." << endl;

                    zeit=clock();

                    tree.DeleteTree();

                    zeit=clock()-zeit;

                    k=k+h;

                    cout << "\tUnd die ben\x94tigte Zeit f\x81r das L\x94schen war: " << zeit << "ms." << endl;

                }

                system("PAUSE");
            }

        }while (menu != 'e');

    }

    catch (exception& e)
    {

        cout << e.what();

    }

}
